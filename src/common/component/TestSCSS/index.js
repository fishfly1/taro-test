import { Component } from 'react'
import { View, Text, Button} from '@tarojs/components'
import './index.scss'


export default class Index extends Component {

  render() {
    return (
      <View>
        <View className='contain layout'>
            <Text className='index-title'>Hello World</Text>
            <Text className='index-title'>Hello World2</Text>
        </View>

        <Text className='index-title'>Hello World3</Text>

        <View className='TestCSS'>
          <Text class='hello'>111111111111111</Text>
          <Text class='hello'>2</Text>
          <Text class='hello'>111111111111111</Text>
          <Text class='hello'>3</Text>
          <Text class='hello'>111111111111111</Text>
          <Text class='hello'>2222222222222222222222222222222222222222222222222</Text>
        </View>
      </View>
    );
  }


}
