import {Button, View, Text} from "@tarojs/components";
import {useState} from "react";


export default function Counter(){

  const [count, setCount] = useState(0);

  function handleAdd(){
    console.log('add', this)
    setCount(count + 1);
  }

  return (
    <View>
      <Text>{count}</Text>
      <Button onClick={handleAdd}>加一</Button>
    </View>
  )

}
