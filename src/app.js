import { Component } from 'react'
import './app.scss'

class App extends  Component {

  globalData = {
    subscribers: [],
  }

  //兼容小程序的写法 https://taro-docs.jd.com/docs/come-from-miniapp#react
  taroGlobalData = {
    globalData: this.globalData
  }

  constructor(props) {
    super(props);
  }

  componentDidMount () {}

  componentDidShow () {}

  componentDidHide () {}

  // this.props.children 是将要会渲染的页面
  render () {
    return this.props.children
  }
}

export default App
