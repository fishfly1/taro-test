

export default function taskReducer(state, action){
  switch(action.type){
    case 'add':
      return [...state, {id: state.length, text: action.text, done: false}];
    case 'toggle':
      return state.map((task) =>
        task.id === action.id ? {...task, done: !task.done} : task
      );
    case 'delete':
      return state.filter((task) => task.id !== action.id);
    default:
      return state;
  }
}
