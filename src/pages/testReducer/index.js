import {useReducer} from 'react';
import {Button, Text, View} from "@tarojs/components";
import taskReducer from "./taskReducer";

const initialTasks = [
  {id: 0, text: '参观卡夫卡博物馆', done: true},
  {id: 1, text: '看木偶戏', done: false},
  {id: 2, text: '打卡列侬墙', done: false}
];

//随机生成一些博物馆的名称
function randomMuseum(){
  const museums = ['卡夫卡博物馆', '列侬墙', '布拉格城堡', '查理大桥', '布拉格老城广场', '布拉格国家剧院', '布拉格犹太城',
    '查理大桥塔', '布拉格国家美术馆']
  return museums[Math.floor(Math.random() * museums.length)];
}

export default function TestReducer(){

  const [tasks, dispatcher] = useReducer(taskReducer, initialTasks);

  return (
    <View>
      <Text>测试Reducer</Text>
      <Button onClick={() => dispatcher({type: 'add', text: randomMuseum()})}>添加任务</Button>
      <Button onClick={() => dispatcher({type: 'delete', id: tasks.length-1})}>删除任务</Button>

      {/*垂直布局*/}
      <View style={{display:"flex", flexDirection:"column"}}>
        {tasks.reverse().map((task) => (
          <Text key={task.id}>
            ID:{task.id}  名称:{task.text}
          </Text>
        ))}
      </View>

    </View>
  )
}
