import {Component, useState, useEffect, useRef} from "react";
import {Button, Text, View, Input, Checkbox} from "@tarojs/components";
import EventEmitter from "@/utils/EventEmitter"
import {useLaunch, useLoad, useDidShow, showModal} from "@tarojs/taro"
import Counter from "@/component/Counter";




function CustomComponent() {

  const [count, setCount] = useState(0);

  const refInput = useRef(null);

  // 相当于 componentDidMount 和 componentDidUpdate:
  useEffect(() => {
    // 使用浏览器的 API 更新页面标题
    document.title = `You clicked ${count} times`;
  });

  useLaunch(()=>{
    console.log('Taro Hook:useLaunch: 等同于 App 入口的 onLaunch 生命周期钩子。')
  })

  useLoad(() => {
    console.log('Taro Hook: onLoad')
  })

  useDidShow(() => {
    console.log('Taro Hook: componentDidShow')
  })

  const onButtonSetInputFocus = () => {
    showModal({
        content:'已设置Input获取焦点', success:()=>{refInput.current.focus()}
      }
    )
  }

  return (
    <View style={{flexDirection:'column', alignContent:'center', background:"greenyellow", borderRadius:20, paddingTop:50, paddingBottom:50}}>
      <Button style={{width:'90%'}} onClick={() => setCount(count+1)}>计数{count}</Button>

      <Button style={{width:'90%'}} onClick={() => onButtonSetInputFocus()}>点击使Input获取焦点</Button>
      <Input ref={refInput} style={{width:'90%', background:'blue'}} placeholder='请输入内容'></Input>
    </View>
  )
}


function SyncedInputs() {

  const [text, setText] = useState('');

  function handleChange(e) {
    setText(e.target.value);
  }

  function MyInput({label, value, onChange}) {
    return (
      <View style={{display:'flex', flexDirection:'row', alignItems:'center'}}>
        <Text style={{fontSize:20}}>
          {label}
        </Text>
        <Input style={{background:'yellow', marginLeft:10}}
          value={value}
          onInput={onChange}
        />
      </View>
    )
  }


  return (
      <View style={{marginTop:50, background:'aliceblue', display:'flex', flexDirection: 'column', justifyContent:'flex-start', alignContent:'center'}}>
        <Text style={{fontSize:20, marginTop:0, background:'red'}}>同步输入测试</Text>
        <MyInput label='第1输入框' value={text} onChange={handleChange} />
        <MyInput label='第2输入框' value={text} onChange={handleChange} />
    </View>
  );
}


function TestCounter() {

  const [showB, setShowB] = useState(true);

  return (
    <View>
      {/*组件有独立的State, 互不影响。
      只要一个组件还被渲染在 UI 树的相同位置，React 就会保留它的 state。
      如果它被移除，或者一个不同的组件被渲染在相同的位置，那么 React 就会丢掉它的 state。*/}
      <Counter />
      {showB && <Counter />}
      <div>
        <Text>显示第二个Counter</Text>
        <Checkbox checked={showB} value={'1'} onClick={e => {
          setShowB(e.target.checked)
        }}
        />
      </div>
    </View>
  )

}


export default class Index extends Component{

  constructor(props) {
    super();
    this.state = {
      propA: 'A',
      propB: 'B'
    }
  }
  componentDidMount() {
    this.setState({propA: 'A+'})
    this.setState({propB: 'B+'})
  }

  componentWillUnmount() {
    this.setState({propA: 'C'})
    console.log('pages/test/index  componentWillUnmount')

  }

  onClick() {
    console.log('pages/test/index  发送事件')
    EventEmitter.dispatch('testEvent', 'testEvent data')
  }

  onClickTestState = () => {
    this.setState({propA: 'C'})
    // this.state.propA = 'A-'
    console.log('pages/test/index  onClickTestState', this.state.propA)
  }

  render() {
    // const {propA=''} = this.state

    return (
      <View>
        <Text>test</Text>
        <Button onClick={this.onClick}>发送事件</Button>
        <CustomComponent />
        <SyncedInputs />
        <Button onClick={this.onClickTestState.bind(this)}>{`测试State异步同步${this.state.propA}`}</Button>
        <Button onClick={this.onClickTestState.bind(this)}>{`测试State异步同步${this.state.propB}`}</Button>
        <TestCounter />

      </View>
    )
  }

}
