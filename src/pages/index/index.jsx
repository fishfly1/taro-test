import React, { Component } from 'react'
import { View, Text, Button} from '@tarojs/components'
import {List} from "react-virtualized";
import {navigateTo, redirectTo, showModal} from '@tarojs/taro'
import EventEmitter from "@/utils/EventEmitter"
import TestSCSS from '@/component/TestSCSS/index'


import './index.scss'

export default class Index extends Component {

  componentDidMount () {
    console.log('pages/index/index  componentDidMount')

    EventEmitter.addListener('testEvent', (data) => {
      console.log('pages/index/index 接收事件 testEvent', data)
    })
  }
  componentWillUnmount () {
    console.log('pages/index/index  componentWillUnmount')

    EventEmitter.removeListener('testEvent')
    console.log('pages/index/index  componentWillUnmount')
  }

  componentDidShow () {
    console.log('pages/index/index  componentDidShow')
  }

  componentDidHide () {
    console.log('pages/index/index  componentDidHide')
  }

  async handleClick_navigateTo() {

    showModal({
      title: '提示',
      content: '点击跳转',
      success: function (res) {
        if (res.confirm) {
          navigateTo({
            url: '/pages/test/index'
          })
        } else if (res.cancel) {
        }
      }

    })
  }

  async handleClick_redirectTo() {
    redirectTo({
      url: '/pages/test/index'
    })
  }

  async handleClick_redirectTo_Reducer(){
    navigateTo({  url: '/pages/testReducer/index' })
  }

  async handleClick_redirectTo_WebCam(){
      navigateTo({  url: '/pages/testWebCam/index' })
  }

  // render () {
  //   // 假设你有一个包含大量数据的列表
  //   const rowCount = 10000;
  //
  //   // 渲染每一行的函数
  //   const rowRenderer = ({ index, key, style }) => (
  //     <div key={key} style={style}>
  //       Row {index}
  //     </div>
  //   );
  //
  //   return (
  //     <View className='index'>
  //       <Text>Hello world!</Text>
  //       <Button type='primary' onClick={this.handleClick_navigateTo}>navigateTo测试</Button>
  //       <Button type='primary' onClick={this.handleClick_redirectTo}>redirectTo测试</Button>
  //       <Button type='primary' onClick={this.handleClick_redirectTo_Reducer}>rnavigateTo Reducer测试</Button>
  //       <Button type='primary' onClick={this.handleClick_redirectTo_WebCam}>rnavigateTo WebCam测试</Button>
  //
  //       <List
  //         width={300} // 列表的宽度
  //         height={600} // 列表的高度
  //         rowCount={rowCount} // 列表项的总数
  //         rowHeight={50} // 每一行的高度
  //         rowRenderer={rowRenderer} // 渲染每一行的函数
  //       />
  //     </View>
  //   )
  // }

  handleClick_packageA(){
    navigateTo({  url: '/packageA/pages/testA1/index' })
  }

  handleClick_packageB(){
    navigateTo({  url: '/packageB/pages/testB1/index' })
  }

  render () {
    return (
      <React.Fragment>
        <View>
          <Button type='primary' onClick={this.handleClick_packageA}>跳转到packageA</Button>
          <Button type='primary' onClick={this.handleClick_packageB}>跳转到packageB</Button>
        </View>

        <TestSCSS></TestSCSS>
      </React.Fragment>

    )
  }
}
