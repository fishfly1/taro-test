export default defineAppConfig({
  pages: [
    'pages/index/index',
    'pages/test/index',
    'pages/testReducer/index',
    'pages/testWebCam/index'
  ],
  subpackages: [
    {
      root: 'packageA',
      pages: [
        'pages/testA1/index',
        'pages/testA2/index'
      ]
    },
    {
      root: 'packageB',
      pages: [
        'pages/testB1/index',
        'pages/testB2/index'
      ]
    }
  ],
  tabBar: {
    color: '#000',
    selectedColor: '#00f',
    backgroundColor: '#fff',
    list: [
      {
        pagePath: 'pages/index/index',
        text: '首页'
      },
      {
        pagePath: 'pages/test/index',
        text: '测试'
      },
      // {
      //   pagePath: 'packageA/pages/testA1/index',
      //   text: 'A1'
      // },
      // {
      //   pagePath: 'packageB/pages/testB1/index',
      //   text: 'B1'
      // }
    ]
  },
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#00f',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  }
})
