module.exports = {
  env: {
    NODE_ENV: '"development"'
  },
  defineConstants: {
  },
  mini: {},
  h5: {
    esnextModules: ['taro-ui'],
    devServer: {
      host: 'localhost',
      port: 10087,
      proxy: {
      }
    }
  }
}
